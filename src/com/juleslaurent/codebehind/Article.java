package com.juleslaurent.codebehind;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Created by Jules LAURENT on 15/03/2017.
 * Last updated on 16/04/17.
 */

public abstract class Article implements IXmlSavable {
    //*********************
    //|      Fields       |
    //*********************

    /**
     * Article identifier ex : CB002
     */
    protected String ref;
    /**
     * Article label
     */
    protected String marque;
    /**
     * Article brand.
     */
    protected String intitule;
    /**
     * Price for one article.
     */
    protected float prixUnit;


    //************************************
    //|      Methods & Constructors      |
    //************************************

    Article()
    {

    }
    /**
     * Fill the instance fields with specified values.
     * @param ref Reference du nouvel article
     * @param marque Marque du nouvel article
     * @param intitule Intitule du nouvel article
     * @param prixUnit Prix unitaire du nouvel article
     */
    Article(String ref, String marque,String intitule, float prixUnit)
    {
        this.ref = ref;
        this.marque = marque;
        this.intitule = intitule;
        this.prixUnit = prixUnit;
    }

    /**
     * Get n. Set the instance fields value from the XML representation.
     * @param element Representation xml d'un article
     */
    Article(Element element)
    {
        this.ref = element.getElementsByTagName("ref").item(0).getTextContent();
        this.marque = element.getElementsByTagName("marque").item(0).getTextContent();
        this.intitule = element.getElementsByTagName("intitule").item(0).getTextContent();
        this.prixUnit = Float.valueOf(element.getElementsByTagName("prix").item(0).getTextContent());
    }

    /**
     * Print the article informations on the CLI.
     */
    public void afficher()
    {
        System.out.println("-----------REF  : " + this.ref + "---------------");
        System.out.println("> Informations produit ");
        System.out.println("Marque : " + this.marque);
        System.out.println("Intitule : " + this.intitule);
        System.out.println("Prix : " + this.getPrixUnit() + " €.pc^-1");
    }

    @Override
    public Element toXml(Document document) {
        Element articleElement = document.createElement("article");

        articleElement.setAttribute("type",this.getClass().getSimpleName());

        Element refElement = document.createElement("ref");
        refElement.appendChild(document.createTextNode(this.getRef()));

        Element marqueElement = document.createElement("marque");
        marqueElement.appendChild(document.createTextNode(this.getMarque()));

        Element intituleElement = document.createElement("intitule");
        intituleElement.appendChild(document.createTextNode(this.getIntitule()));

        Element prixUnitElement = document.createElement("prix");
        prixUnitElement.appendChild(document.createTextNode(String.valueOf(this.getPrixUnit())));

        articleElement.appendChild(refElement);
        articleElement.appendChild(marqueElement);
        articleElement.appendChild(intituleElement);
        articleElement.appendChild(prixUnitElement);

        return articleElement;
    }

    @Override
    public String toString() {
        return  this.intitule + "#" + this.ref;
    }

    //*********************
    //| Getters & Setters |
    //*********************

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public float getPrixUnit() {
        return prixUnit;
    }

    public void setPrixUnit(float prixUnit) {
        this.prixUnit = prixUnit;
    }

    public String getRef() {
        return ref;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }
}



