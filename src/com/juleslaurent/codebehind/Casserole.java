package com.juleslaurent.codebehind;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.ArrayList;

/**
 * Created by Jules LAURENT on 15/03/2017.
 * Last updated on 16/04/17.
 */
public class Casserole extends Article {

    //*********************
    //|      Fields       |
    //*********************

    /**
     * Maximum supported temperature.
     */
    private float tMax;


    //************************************
    //|      Methods & Constructors      |
    //************************************

    public Casserole()
    {
        super("#########","Marque","Intitule",0F);
        this.tMax = 0F;
    }

    /**
     * @see Article#Article(String, String, String, float)
     */
    public Casserole(String ref,String marque, String intitule, float prixUnit, float tMax) {
        super(ref,marque,intitule, prixUnit);
        this.tMax = tMax;
    }


    /**
     * @see Article#Article(Element)
     */
    Casserole(Element element)
    {
        super(element);
        this.tMax = Float.valueOf(element.getElementsByTagName("tmax").item(0).getTextContent());
    }

    @Override
    public void afficher() {
        super.afficher();
        System.out.println("T max" + tMax);
    }

    @Override
    public Element toXml(Document document) {
        Element articleElement = super.toXml(document);

        Element tMaxElement = document.createElement("tmax");
        tMaxElement.appendChild(document.createTextNode(String.valueOf(this.gettMax())));
        articleElement.appendChild(tMaxElement);
        return articleElement;
    }

    //*********************
    //| Getters & Setters |
    //*********************
    public float gettMax() {
        return tMax;
    }

    public void settMax(float tMax) {
        this.tMax = tMax;
    }

}
