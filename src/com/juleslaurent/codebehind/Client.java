package com.juleslaurent.codebehind;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Created by Jules LAURENT on 03/04/2017.
 * Last updated on 16/04/17.
 */
public class Client implements IXmlSavable{

    //*********************
    //|      Fields       |
    //*********************

    /**
     * client primary identifier
     */
    private String numero;
    /**
     * client last name
     */
    private String nom;
    /**
     * client first name
     */
    private String prenom;
    /**
     * client living town - Standing for the full address.
     */
    private String ville;

    /**
     * Create an empty client. Used in gui.
     */

    //************************************
    //|      Methods & Constructors      |
    //************************************
    public Client()
    {
        this("#######","JACQUE","DELARUELLEDUBAS","RAINBOWCITY");
    }

    /**
     *
     * @param numero new client identifier.
     * @param nom new client last name.
     * @param prenom new client first name.
     * @param ville new client living town.
     */
    public Client(String numero, String nom, String prenom, String ville)
    {
        this.numero = numero;
        this.nom = nom;
        this.prenom = prenom;
        this.ville = ville;
    }

    /**
     * Create a client from an XML representation.
     * @param element new client xml representation.
     */
    public Client(Element element)
    {
        this.numero = element.getElementsByTagName("numero").item(0).getTextContent();
        this.nom = element.getElementsByTagName("nom").item(0).getTextContent();
        this.prenom = element.getElementsByTagName("prenom").item(0).getTextContent();
        this.ville = element.getElementsByTagName("ville").item(0).getTextContent();
    }

    @Override
    public String toString() {
        return this.getNom() + " " + this.getPrenom();
    }


    @Override
    public Element toXml(Document document)
    {
        Element nomx = document.createElement("nom");
        nomx.appendChild(document.createTextNode(this.getNom()));

        Element prenomx = document.createElement("prenom");
        prenomx.appendChild(document.createTextNode(this.getPrenom()));

        Element villex = document.createElement("ville");
        villex.appendChild(document.createTextNode(this.getVille()));

        Element numerox = document.createElement("numero");
        numerox.appendChild(document.createTextNode(this.getNumero()));

        Element client = document.createElement("client");
        client.appendChild(numerox);
        client.appendChild(nomx);
        client.appendChild(prenomx);
        client.appendChild(villex);

        return client;
    }

    //*********************
    //| Getters & Setters |
    //*********************

    public String getNumero() {
        return numero;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }
}
