package com.juleslaurent.codebehind;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.ArrayList;

/**
 * Created by Jules LAURENT on 15/03/2017.
 */
public class Couvert extends Article
{
    private Matiere matiere;
    public Couvert()
    {
        this("#########","Marque","Intitule",0F,Matiere.Aluminium);
    }

    public Couvert(String ref,  String marque, String intitule, float prixUnit, Matiere matiere) {
        super(ref,marque, intitule ,prixUnit);
        this.matiere = matiere;
    }
    Couvert(Element element)
    {
        super(element);
        this.matiere = Matiere.valueOf(element.getElementsByTagName("matiere").item(0).getTextContent());
    }

    @Override
    public void afficher() {
        super.afficher();
        System.out.println("Matiere : " + this.matiere);
    }

    // Getters & Setters
    public Matiere getMatiere() {
        return matiere;
    }

    public void setMatiere(Matiere matiere) {
        this.matiere = matiere;
    }

    @Override
    public Element toXml(Document document) {
        Element articleElement = super.toXml(document);

        Element matiereElement = document.createElement("matiere");
        matiereElement.appendChild(document.createTextNode(this.getMatiere().toString()));
        articleElement.appendChild(matiereElement);
        return articleElement;
    }
}
