package com.juleslaurent.codebehind;

/**
 * Throwed when you try to add something with an already existent identifier.
 * Created by Jules LAURENT on 11/04/2017.
 * Last updated on 16/04/17.
 */
public class DuplicateException extends Exception {
    DuplicateException()
    {
        super();
    }

    /**
     * Include the relevant informations in the exception message.
     * @param type string representation of the duplicate object type.
     * @param id duplicate identifier.
     */
    DuplicateException(String type,String id)
    {
        super("Un objet de type " + type + "  portant l'identifiant " + id + " existe déjà !");
    }
}
