package com.juleslaurent.codebehind;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Jules LAURENT on 03/04/2017.
 * Last updated on 16/04/17.
 */
public class Facture implements IXmlSavable {
    //*********************
    //|      Fields       |
    //********************
    /**
     * Contain all the items that compose the total bill amount.
     */
    private ArrayList<ItemFacture> articlesFactures = new ArrayList<ItemFacture>();
    /**
     * Bill creation date.
     */
    private Date date = new Date();
    /**
     * Paying customer.
     */
    private Client client;
    /**
     * Total amount to pay.
     */
    private float montantTotal = 0;
    /**
     * Bill identifier.
     */
    private String reference;


    //************************************
    //|      Methods & Constructors      |
    //************************************

    public Facture()
    {

    }

    /**
     * Create an empty bill with a reference n. a client
     * @param reference new bill identifier.
     * @param client paying customer of the new bill.
     */
    Facture(String reference,Client client)
    {
        this.reference = reference;
        this.client = client;
    }

    /**
     * Create a bill wich already contains items and have a creation date different than now.
     * @param reference bill reference.
     * @param date bill date.
     * @param client bill paying customer.
     * @param itemFactures billed items array.
     */
    public Facture(String reference, Date date, Client client,ArrayList<ItemFacture> itemFactures)
    {
        this(reference,client);
        this.date = date;
        this.articlesFactures = itemFactures;
    }

    /**
     * Restore a bill from a XML representation.
     * @param element XML representation.
     */
    Facture(Element element)
    {
        this(element.getElementsByTagName("reference").item(0).getTextContent(),
         Magasin.getInstance().getClientByNumero(element.getElementsByTagName("client").item(0).getTextContent()));
        //Parsing the date string.
        Date date1 = new Date();
        try
        {
            DateFormat df = new SimpleDateFormat("EEE MMM dd kk:mm:ss ZZZZ yyyy", Locale.US);
            date1 = df.parse("Tue Apr 11 15:10:33 CEST 2017");

        }catch (ParseException ex)
        {
            ex.printStackTrace();
        }


        ArrayList<ItemFacture> itemFacturesTmp = new ArrayList<>();
        //Restoring and adding the billed itemps.
        NodeList itemFacturesSav = ((Element)element.getElementsByTagName("items").item(0)).getElementsByTagName("item");
        for(int i = 0; i < itemFacturesSav.getLength();i++)
        {
            itemFacturesTmp.add(new ItemFacture((Element)itemFacturesSav.item(i)));
        }
        this.articlesFactures = itemFacturesTmp;
        this.date = date1;
    }

    @Override
    public Element toXml(Document document) {
        Element refElement = document.createElement("reference");
        refElement.appendChild(document.createTextNode(this.getReference()));

        Element dateElement = document.createElement("date");
        dateElement.appendChild(document.createTextNode(this.getDate().toString()));

        Element clientElement = document.createElement("client");
        clientElement.appendChild(document.createTextNode(this.getClient().getNumero()));

        Element itemsElements = document.createElement("items");
        for(ItemFacture item : this.getArticlesFactures())
        {
            itemsElements.appendChild(item.toXml(document));
        }

        Element factureElement = document.createElement("facture");
        factureElement.appendChild(refElement);
        factureElement.appendChild(dateElement);
        factureElement.appendChild(clientElement);
        factureElement.appendChild(itemsElements);

        return  factureElement;
    }


    //*********************
    //| Getters & Setters |
    //*********************

    public ArrayList<ItemFacture> getArticlesFactures() {
        return articlesFactures;
    }

    public String getReference() {
        return reference;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * Recalculate and return the total amount charged.
     * @return Float - the total amount charged.
     */
    public float getMontantTotal() {
        System.out.println(this.articlesFactures.size());
        float total = 0 ;
        for(ItemFacture itemFacture : this.articlesFactures)
        {
            total += itemFacture.getNbItems() * itemFacture.getArticle().getPrixUnit();
        }
        this.montantTotal = total;
        return montantTotal;
    }



}
