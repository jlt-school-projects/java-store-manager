package com.juleslaurent.codebehind;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Exportable to XML objects.
 * Created by Jules LAURENT on 10/04/2017.
 * Last updated on 16/04/17.
 */

public interface IXmlSavable {
    /**
     * Export the target object to XML representation.
     * @param document lambda document instance. (Used to create new elements)
     * @return Element containing the XML representation of the target object.
     */
    Element toXml(Document document);

}

