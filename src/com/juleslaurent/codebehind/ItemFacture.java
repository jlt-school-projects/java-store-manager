package com.juleslaurent.codebehind;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *  Created by Jules LAURENT on 04/04/2017.
 * 	Last updated on 16/04/17.
 */
public class ItemFacture implements IXmlSavable {
    //*********************
    //|      Fields       |
    //*********************
    /**
     * Number of items
     */
    private int nbItems;
    /**
     * Article
     */
    private Article article;

    public ItemFacture() {
        this(0, new Casserole());
    }

    /**
     * Create a new set of articles.
     * @param nbItems number of items
     * @param article article.
     */
    ItemFacture(int nbItems, Article article) {
        this.nbItems = nbItems;
        this.article = article;
    }

    /**
     * Restore a set of articles from a XML representation.
     * @param element XML representation.
     */
    ItemFacture(Element element) {
        this(Integer.valueOf(element.getElementsByTagName("quantite").item(0).getTextContent()),
                Magasin.getInstance().getArticleByRef(element.getElementsByTagName("article").item(0).getTextContent()));
    }
    //************************************
    //|      Methods & Constructors      |
    //************************************

    @Override
    public Element toXml(Document document) {
        Element articleElement = document.createElement("article");
        articleElement.appendChild(document.createTextNode(this.getArticle().getRef()));

        Element quantiteElement = document.createElement("quantite");
        quantiteElement.appendChild(document.createTextNode(String.valueOf(this.getNbItems())));

        Element itemElement = document.createElement("item");
        itemElement.appendChild(articleElement);
        itemElement.appendChild(quantiteElement);

        return itemElement;
    }

    //*********************
    //| Getters & Setters |
    //*********************
    public int getNbItems() {
        return nbItems;
    }

    public void setNbItems(int nbItems) {
        this.nbItems = nbItems;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }
}
