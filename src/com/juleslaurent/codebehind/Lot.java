package com.juleslaurent.codebehind;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Created by Jules LAURENT on 15/03/2017.
 * Last updated on 16/04/17.
 */
public class Lot extends Article{
    //*********************
    //|      Fields       |
    //*********************
    /**
     * Discount percentage.
     */
    private float ratioReduction;
    /**
     * Number of items in the package.
     */
    private int nbArticles;
    /**
     * Article packaged.
     */
    Article article;


    //************************************
    //|      Methods & Constructors      |
    //************************************

    /**
     * Create an empty Lot object. used in the gui.
     */
    public Lot()
    {
        this(" ",Magasin.getInstance().getArticles().get(0),0F,0);
    }

    /**
     * Create a new package.
     * @param ref package identifier.
     * @param article package packaged article.
     * @param ratioReduction package discount percentage.
     * @param nbArticles package articles items.
     */
    public Lot(String ref, Article article, float ratioReduction, int nbArticles) {
        super(ref ,article.getMarque(),"Lot de " + article.getIntitule(),0);
        this.article = article;
        this.ratioReduction= ratioReduction;
        this.nbArticles = nbArticles;
        this.prixUnit = this.calculPrixUnit(this.article.getPrixUnit(),this.ratioReduction,this.nbArticles);

    }

    /**
     * @see Article#Article(Element)
     *
     */
    Lot(Element element)
    {
        this(element.getElementsByTagName("ref").item(0).getTextContent(),
                Magasin.getInstance().getArticleByRef(element.getElementsByTagName("article").item(0).getTextContent()),
                Float.valueOf(element.getElementsByTagName("ratio").item(0).getTextContent()),
                Integer.valueOf(element.getElementsByTagName("nbarticles").item(0).getTextContent()));
}

    /**
     * Recalculate and return the package price.
     * @return the package price.
     */
    @Override
    public float getPrixUnit() {
        this.prixUnit = this.calculPrixUnit(this.article.prixUnit,this.ratioReduction,this.nbArticles);
        return this.prixUnit;
    }

    /**
     * Reformat the label with "Lot de" and return it.
     * @return the label formatted with "Lot de ".
     */
    @Override
    public String getIntitule() {
        this.intitule = "Lot de " + this.article.getIntitule();
        return this.intitule;
    }

    @Override
    public void afficher() {
        super.afficher();
        System.out.println("> Information lot");
        System.out.println("Nombre d'Articles : " + nbArticles);
        System.out.println("Reduction : " + ratioReduction + "%");
    }

    /**
     * Caculate the package price depending on the discount ant the number ot items.
     * @param prixUnit price of one item of the package.
     * @param ratio package discount percentage.
     * @param nbArticles number of items in the package.
     * @return the calculated price of the package.
     */
    public float calculPrixUnit(float prixUnit, float ratio,int nbArticles)
    {
        float ratioReel = (100- ratio)/100;
        return (nbArticles * prixUnit) * ratioReel;
    }

    @Override
    public Element toXml(Document document)
    {
        Element articleElement = super.toXml(document);

        Element ratioElement = document.createElement("ratio");
        ratioElement.appendChild(document.createTextNode(String.valueOf(this.getRatioReduction())));

        Element nbArticlesElement = document.createElement("nbarticles");
        nbArticlesElement.appendChild(document.createTextNode(String.valueOf(this.getNbArticles())));

        Element artElement = document.createElement("article");
        artElement.appendChild(document.createTextNode(this.getArticle().getRef()));

        articleElement.appendChild(artElement);
        articleElement.appendChild(ratioElement);
        articleElement.appendChild(nbArticlesElement);

        return articleElement;
    }

    //*********************
    //| Getters & Setters |
    //*********************

    public float getRatioReduction() {
        return ratioReduction;
    }

    public void setRatioReduction(float ratioReduction) {
        this.ratioReduction = ratioReduction;
    }

    public int getNbArticles() {
        return nbArticles;
    }

    public void setNbArticles(int nbArticles) {
        this.nbArticles = nbArticles;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

}
