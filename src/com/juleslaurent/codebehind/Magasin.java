package com.juleslaurent.codebehind;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import org.w3c.dom.*;

/**
 * Center of operations. Saving is processed here.
 * Created by Jules LAURENT on 17/03/2017.
 * Last updated on 16/04/17.
 */
public class Magasin {
    //*********************
    //|      Fields       |
    //*********************

    /**
     * Store(Magasin) singleton instance.
     */
    private static Magasin ourInstance = new Magasin();

    /**
     *
     * @return the store (Magasin) instance.
     */
    public static Magasin getInstance() {
        return ourInstance;
    }

    /**
     *      Contain the articles of the store.
     */
    private ArrayList<Article> articles = new ArrayList<Article>();
    /**
     *      Contain the clients of the store.
     */
    private ArrayList<Client> clients = new ArrayList<Client>();
    /**
     *      Contain the bills of the stores.
     */
    private ArrayList<Facture> factures = new ArrayList<Facture>();

    /**
     * Name of the store.
     */
    private String nom;


    //************************************
    //|      Methods & Constructors      |
    //************************************

    public ArrayList<Article> getArticles() {
        return articles;
    }

    /**
     *
     * Return the article from the store list with the desired reference.
     * @param ref Desired reference.
     * @return Article object with the desired reference or null.
     */
    public Article getArticleByRef(String ref) {
        for (Article article : this.articles) {
            if (article.getRef().equals(ref)) {
                return article;
            }
        }
        return null;
    }

    /**
     *
     * Allow us to add an article to the store list without creating any double match.
     * @param article article to add.
     * @throws DuplicateException
     */
    public void addArticle(Article article) throws DuplicateException {
        if (this.getArticleByRef(article.getRef()) == null) {
            this.articles.add(article);
        } else {
            throw new DuplicateException(article.getClass().toString(), article.getRef());
        }
    }

    public ArrayList<Client> getClients() {
        return clients;
    }

    /**
     *
     * Return the client from the store's list with the desired client number.
     * @param numero desired client number.
     * @return Client object with the desired number.
     */
    public Client getClientByNumero(String numero) {
        for (Client client : this.clients) {
            if (client.getNumero().equals(numero)) {
                return client;
            }
        }
        return null;
    }

    /**
     * Allow us to add a client to the store's list without creating any double match.
     * @param client client to add.
     * @throws DuplicateException if a client with the same reference already exists.
     */
    public void addClient(Client client) throws DuplicateException {
        if (getClientByNumero(client.getNumero()) == null) {
            this.clients.add(client);
        } else {
            System.out.println("Duplicate");
            throw new DuplicateException(Client.class.toString(), client.getNumero());
        }
    }

    public ArrayList<Facture> getFactures() {
        return factures;
    }

    /**
     * Return the bill with the desired bill reference.
     * @param reference desired bill reference.
     * @return a Facture object with the desired reference.
     */
    public Facture getFactureByReference(String reference) {
        for (Facture facture : this.factures) {
            if (facture.getReference().equals(reference)) {
                return facture;
            }
        }
        return null;
    }

    /**
     * Allow us to add a bill to the store's list without creating any double match.
     * @param facture bill to add.
     * @throws DuplicateException if a bill with the same reference already exists.
     */
    public void addFacture(Facture facture) throws DuplicateException {
        if (getFactureByReference(facture.getReference()) == null) {
            this.factures.add(facture);
        } else {
            throw new DuplicateException(facture.getClass().toString(), facture.getReference());
        }
    }


    private Magasin() {

    }

    /**
     * Save the all store (articles,clients n. bills) in the record.xml file.
     */
    public static void save() {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            org.w3c.dom.Element rootElement = doc.createElement("magasin");
            doc.appendChild(rootElement);

            Element clientsElement = doc.createElement("clients");
            for (Client client : Magasin.getInstance().getClients()) {
                clientsElement.appendChild(client.toXml(doc));
            }
            rootElement.appendChild(clientsElement);

            Element articlesElement = doc.createElement("articles");
            for (Article article : Magasin.getInstance().getArticles()) {
                articlesElement.appendChild(article.toXml(doc));
            }
            rootElement.appendChild(articlesElement);

            Element facturesElement = doc.createElement("factures");
            for(Facture facture : Magasin.getInstance().getFactures())
            {
                facturesElement.appendChild(facture.toXml(doc));
            }
            rootElement.appendChild(facturesElement);

            //Getting the tools to transform the doc
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            //to transform in a proper indented file.
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            DOMSource source = new DOMSource(doc);

            //Getting the stream.
            StreamResult console = new StreamResult(System.out);
            StreamResult file = new StreamResult(new File("res/record.xml"));

            //write data in the stream.
            transformer.transform(source, console);
            transformer.transform(source, file);
            System.out.println("DONE");

        } catch (Exception e) {
            System.out.println("Une erreur est survenue lors de la sauvegarde.1 ");
            System.out.println(e);
            e.printStackTrace();
        }

    }

    /**
     * Restore all the store (articles, clients n. bills) from the record.xml file.
     */
    public static void fromXml() {
        try {
            //Getting and parsing the file into an Xml document object.
            //String filePath = ;
           // File xmlFile = new File(filePath);
            DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbfactory.newDocumentBuilder();
            Document doc = db.parse(Magasin.class.getResourceAsStream("/record.xml"));


            Element rootElement = doc.getDocumentElement();

            //Getting all the articles.
            NodeList articles = ((Element) rootElement.getElementsByTagName("articles").item(0)).getElementsByTagName("article");
            for (int i = 0; i < articles.getLength(); i++) {
                try {
                    switch (((Element) articles.item(i)).getAttribute("type"))
                    //Restoring the articles by real type with the dedicated constructor.
                    {
                        case "Casserole":
                            getInstance().addArticle(new Casserole((Element) articles.item(i)));
                        case "Couvert":
                            getInstance().addArticle(new Couvert((Element) articles.item(i)));
                        case "Lot":
                            getInstance().addArticle(new Lot((Element)articles.item(i)));
                        default:
                            System.out.println("Type inconnu : le fichier de sauvegarde est peut etre corrompu");
                    }
                }catch (DuplicateException ex)
                {
                    System.out.println(ex);
                }
            }

            //Getting all the clients.
            NodeList clients = ((Element) rootElement.getElementsByTagName("clients").item(0)).getElementsByTagName("client");

            //Restoring the clients from xml via the dedicated Client overloaded constructor.
            for (int i = 0; i < clients.getLength(); i++) {
                try {
                    getInstance().addClient(new Client((Element) clients.item(i)));
                } catch (DuplicateException ex) {
                    System.out.println(ex);
                }
            }

            //Getting all the bills.
            NodeList factures = ((Element) rootElement.getElementsByTagName("factures").item(0)).getElementsByTagName("facture");

            //Restoring the bills from xml via the dedicated Facture overloaded constructor.
            for (int i = 0; i < factures.getLength(); i++) {
                try {
                    getInstance().addFacture(new Facture((Element) factures.item(i)));
                } catch (DuplicateException ex) {
                    System.out.println(ex);
                }
            }


        } catch (Exception e) {
            System.out.println("Une ereur est survenue");
            System.out.println(e.toString());
            e.printStackTrace();
        }


    }


    //*********************
    //| Getters & Setters |
    //*********************

    /**
     * Return the name of the store.
     * @return the name of the store.
     */
    public String getNom() {
        return nom;
    }

    /**
     * Set the name of the store.
     * @param nom new name of the store.
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

}
