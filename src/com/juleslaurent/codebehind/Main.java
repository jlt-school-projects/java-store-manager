package com.juleslaurent.codebehind;

import com.juleslaurent.gui.*;

/**
 * Created by Jules LAURENT on 15/03/2017.
 */
public class Main {
    public static void main(String[] args) {

        Magasin.getInstance().setNom("Jules LAURENT");

        try {
            Magasin.getInstance().addArticle(new Couvert("COUV001", "FishAye", "Couteau a poisson", 12.0F, Matiere.Inox));
            Magasin.getInstance().addArticle(new Casserole("CA001", "T-Fale", "Casserole Haute King Size", 45.0F, 250));
            Magasin.getInstance().addArticle(new Lot("LOT001", Magasin.getInstance().getArticles().get(1), 10.0F, 12));
        }catch (DuplicateException ex)
        {
            System.out.println(ex);
        }

        Client jm = new Client("CL123","Jean marc","Généreux","Pauvreté");
        Magasin.getInstance().getClients().add(jm);
        Magasin.getInstance().getFactures().add(new Facture("FA001",jm));
        for(Article article:Magasin.getInstance().getArticles())
        {
            article.afficher();
        }
        Magasin.getInstance().getArticles().get(1).setPrixUnit(100.0F);
        MotherWindow motherWindow = new  MotherWindow();
        Magasin.fromXml();
        Magasin.save();
        motherWindow.refresh();
    }
}
