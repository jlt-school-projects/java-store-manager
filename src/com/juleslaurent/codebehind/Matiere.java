package com.juleslaurent.codebehind;

/**
 * Created by Jules LAURENT on 17/03/2017.
 * Last updated on 16/04/17.
 */
public enum Matiere {
    Inox,
    Bois,
    Aluminium,
    Ceramique;

}
