package com.juleslaurent.gui;

import com.juleslaurent.codebehind.Article;
import com.juleslaurent.codebehind.DuplicateException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;

/**
 * Generic view specified for Article type objects.
 * Created by Jules LAURENT on 25/03/2017.
 * Last updated on 16/04/17.
 */
public abstract class ArticleView extends View {
    /**
     * Article to display.
     */
    protected Article articleEnLot;
    /**
     * Article identifier field.
     */
    protected JTextField refField;
    /**
     * Article brand field.
     */
    protected JTextField marqueField;
    /**
     * Article label field.
     */
    protected JTextField intituleField;
    /**
     * Article price field.
     */
    protected MyFloatField prixUField;

    ArticleView(){

    }

    ArticleView(Article articleEnLot, Boolean isNew)
    {
        //Setting up the frame title.
        if(isNew)
            this.setTitle("Nouv. " + articleEnLot.getClass().getSimpleName());
        else
            this.setTitle(articleEnLot.getClass().getSimpleName() + " #" + articleEnLot.getRef());

        this.articleEnLot = articleEnLot;
        this.isNew = isNew;

        this.refField = new JTextField(articleEnLot.getRef());
        this.refField.setEditable(isNew);
        MyLinePanel refPanel  = new MyLinePanel();
        refPanel.add(new Component[]{new MyLabel("Référence"),refField});

        this.intituleField = new JTextField(articleEnLot.getIntitule());
        MyLinePanel intitulePanel = new MyLinePanel();
        intitulePanel.add(new Component[]{new MyLabel("Intitule"),intituleField});

        this.marqueField = new JTextField(articleEnLot.getMarque());
        MyLinePanel marquePanel = new MyLinePanel();
        marquePanel.add(new Component[]{new MyLabel("Marque"),marqueField});

        this.prixUField = new MyFloatField(String.valueOf(articleEnLot.getPrixUnit()));
        MyLinePanel prixUPanel = new MyLinePanel();
        prixUPanel.add(new Component[]{new MyLabel("PrixUnitaire"),prixUField});

        this.form.add(new Component[]{refPanel,intitulePanel,marquePanel,prixUPanel});

        pack();
    }

    ArticleView(Article article)
    {
        this(article,false);
    }

    @Override
    public void saveChanges() throws DuplicateException
    {
        this.articleEnLot.setIntitule(this.intituleField.getText());
        this.articleEnLot.setMarque(this.marqueField.getText());
        System.out.println(Float.valueOf(prixUField.getText()));
        this.articleEnLot.setPrixUnit(Float.valueOf(this.prixUField.getText()));

        super.saveChanges();
    }

}
