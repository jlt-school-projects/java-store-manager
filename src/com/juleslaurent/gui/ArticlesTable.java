package com.juleslaurent.gui;
import com.juleslaurent.codebehind.*;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
/**
 * Table model to display article.
 * Created by Jules LAURENT on 19/03/2017.
 * Last updated on 16/04/17.
 */
public class ArticlesTable extends MyAbstractTableModel {

    ArticlesTable()
    {
       this.items = Magasin.getInstance().getArticles();
    }
    @Override
    public int getRowCount() {
        return this.items.size();
    }

    @Override
    public Object getValueAt(int i, int i1) {
        switch (i1)
        {
            case 0:
                return ((Article)this.items.get(i)).getRef();
            case 1:
                return ((Article)this.items.get(i)).getMarque();
            case 2 :
                return ((Article)this.items.get(i)).getIntitule();
            case 3 :
                return ((Article)this.items.get(i)).getPrixUnit();
            default:
                return null;
        }
    }
    @Override
    public Class<?> getColumnClass(int i) {
        return super.getColumnClass(i);
    }

    @Override
    public int getColumnCount() {
        return Article.class.getDeclaredFields().length;
    }

    @Override
    public String getColumnName(int i) {
        return Article.class.getDeclaredFields()[i].toString().substring(Article.class.getDeclaredFields()[i].toString().lastIndexOf('.') + 1).toUpperCase();
    }
}
