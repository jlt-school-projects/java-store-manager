package com.juleslaurent.gui;

import com.juleslaurent.codebehind.Client;
import com.juleslaurent.codebehind.DuplicateException;
import com.juleslaurent.codebehind.Facture;
import com.juleslaurent.codebehind.Magasin;
import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Generic view for the bills objects.
 * Created by Jules LAURENT on 05/04/2017.
 */
public class BillView extends View {
    private JTextField referenceField = new JTextField();
    private JTextField dateField = new JTextField();
    private JComboBox clientJComboBox;
    private MyFloatField totalField;
    private Facture facture;

    BillView(Facture facture, boolean isNew) {
        if(isNew)
            this.setTitle("Nouvelle facture");
        else
            this.setTitle("Facture n° " + facture.getReference());

        this.isNew = isNew;
        this.facture = facture;
        this.referenceField.setText(facture.getReference());
        this.referenceField.setEditable(this.isNew);
        this.dateField.setText(facture.getDate().toString());
        this.dateField.setEditable(false);
        this.clientJComboBox = new JComboBox(Magasin.getInstance().getClients().toArray());
        if (!isNew)
            this.clientJComboBox.setSelectedItem(facture.getClient());
        this.totalField = new MyFloatField(String.valueOf(facture.getMontantTotal()));
        this.totalField.setEditable(false);

        MyLinePanel referencePanel = new MyLinePanel();
        referencePanel.add(new Component[]{new MyLabel("Reference"), referenceField});
        MyLinePanel datePanel = new MyLinePanel();
        datePanel.add(new Component[]{new MyLabel("Date"), dateField});
        MyLinePanel clientPanel = new MyLinePanel();
        clientPanel.add(new Component[]{new MyLabel("Client"), clientJComboBox});
        MyLinePanel totalPanel = new MyLinePanel();
        totalPanel.add(new Component[]{new MyLabel("Montant total "), totalField});


        this.form.add(new Component[]{referencePanel, datePanel, clientPanel, new ItemFacturePane(new ItemFactureTable(this.facture)), totalPanel});

        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {

            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {
                refresh();
            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {
                refresh();
            }
        });
        pack();
    }

    BillView(Facture facture) {
        this(facture, false);
    }

    @Override
    public void saveChanges() throws DuplicateException {
        this.facture.setClient((Client) this.clientJComboBox.getSelectedItem());
        if (this.isNew) {
            Magasin.getInstance().addFacture(new Facture(this.referenceField.getText(), this.facture.getDate(),
                    this.facture.getClient(), this.facture.getArticlesFactures()));
        }
        super.saveChanges();
    }

    public void refresh() {
        this.totalField.setText(String.valueOf(this.facture.getMontantTotal()));
        System.out.println(this.facture.getMontantTotal());
        pack();
    }
}
