package com.juleslaurent.gui;

import com.juleslaurent.codebehind.Facture;
import com.juleslaurent.codebehind.Magasin;

/**
 * Table model to display all the bills.
 * Created by Jules LAURENT on 03/04/2017.
 * Last updated on 16/04/17.
 */
public class BillsTable extends MyAbstractTableModel{
    BillsTable()
    {
        this.names = new String[]{"Reference facture","Client","Montant"};
        this.items = Magasin.getInstance().getFactures();
    }
    @Override
    public int getRowCount() {
        return this.items.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int i) {
        return this.names[i];
    }

    @Override
    public Object getValueAt(int i, int i1) {
        switch (i1)
        {
            case 0:
                return ((Facture)this.items.get(i)).getReference();
            case 1:
                return ((Facture)this.items.get(i)).getClient().getNom() + " " +  ((Facture)this.items.get(i)).getClient().getPrenom();
            case 2:
                return ((Facture)this.items.get(i)).getMontantTotal();
            default:
                return null;
        }
    }
}
