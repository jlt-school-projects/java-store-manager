package com.juleslaurent.gui;

import com.juleslaurent.codebehind.Casserole;
import com.juleslaurent.codebehind.DuplicateException;
import com.juleslaurent.codebehind.Magasin;

import javax.swing.*;
import java.awt.*;

/**
 * Generic view for Caserole with specific fields.
 * Created by Jules LAURENT on 27/03/2017.
 */
public class CasseroleView extends ArticleView {
    CasseroleView() {

    }

    private MyFloatField tMaxField;

    CasseroleView(Casserole article) {
        this(article, false);
    }

    CasseroleView(Casserole article, boolean isNew) {
        super(article, isNew);
        tMaxField = new MyFloatField(String.valueOf(article.gettMax()));
        MyLinePanel tMaxPanel = new MyLinePanel();
        tMaxPanel.add(new Component[]{new MyLabel("Température"), tMaxField});
        this.form.add(tMaxPanel);
        pack();


    }

    @Override
    public void saveChanges() throws DuplicateException {

        if (this.isNew) {
            Magasin.getInstance().addArticle(new Casserole(this.refField.getText(), this.intituleField.getText(),
                    this.marqueField.getText(), Float.valueOf(this.prixUField.getText()), Float.valueOf(this.tMaxField.getText())));
        }
        ((Casserole) this.articleEnLot).settMax(Float.valueOf(this.tMaxField.getText()));
        super.saveChanges();

    }
}
