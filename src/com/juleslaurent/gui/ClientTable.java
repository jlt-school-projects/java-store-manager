package com.juleslaurent.gui;

import com.juleslaurent.codebehind.Client;
import com.juleslaurent.codebehind.Magasin;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

/**
 * Table model to display  clients.
 * Created by Jules LAURENT on 03/04/2017.
 * Last updated on 16/04/17.
 */
public class ClientTable extends MyAbstractTableModel
{

    ClientTable()
    {
        this.names = new String[]{"ID","NOM","PRENOM","VILLE"};
        this.items = Magasin.getInstance().getClients();

    }
    @Override
    public int getRowCount() {
        return this.items.size();
    }

    @Override
    public int getColumnCount() {
        return Client.class.getDeclaredFields().length;
    }

    @Override
    public String getColumnName(int i) {
        return this.names[i];
    }

    @Override
    public Object getValueAt(int i, int i1) {
        switch(i1)
        {
            case 0:
                return ((Client)this.items.get(i)).getNumero();
            case 1:
                return ((Client)this.items.get(i)).getNom();
            case 2:
                return ((Client)this.items.get(i)).getPrenom();
            case 3:
                return ((Client)this.items.get(i)).getVille();
            default:
                return null;
        }
    }
}