package com.juleslaurent.gui;

import com.juleslaurent.codebehind.Client;
import com.juleslaurent.codebehind.DuplicateException;
import com.juleslaurent.codebehind.Magasin;

import javax.swing.*;
import java.awt.*;

/**
 * Generic view for Client objects.
 * Created by Jules LAURENT on 04/04/2017.
 * Last updated on 16/04/17.
 */
public class ClientView extends View {

    private Client client;
    private JTextField numeroField = new JTextField();
    private JTextField nomField = new JTextField();
    private JTextField prenomField = new JTextField();
    private JTextField villeField = new JTextField();


    public ClientView(Client client, Boolean isNew) {
        if(isNew)
            this.setTitle("Nouveau client");
        else
            this.setTitle("Client n°" + client.getNumero());

        this.isNew = isNew;
        this.client = client;
        this.numeroField.setText(this.client.getNumero());
        this.numeroField.setEditable(this.isNew);
        this.nomField.setText(this.client.getNom());
        this.prenomField.setText(this.client.getPrenom());
        this.villeField.setText(this.client.getVille());

        MyLinePanel numeroPanel = new MyLinePanel();
        numeroPanel.add(new Component[]{new MyLabel("Numero client"), this.numeroField});
        MyLinePanel nomPanel = new MyLinePanel();
        nomPanel.add(new Component[]{new MyLabel("NOM"), this.nomField});
        MyLinePanel prenomPanel = new MyLinePanel();
        prenomPanel.add(new Component[]{new MyLabel("Prenom"), this.prenomField});
        MyLinePanel villePanel = new MyLinePanel();
        villePanel.add(new Component[]{new MyLabel("Ville"), this.villeField});

        this.form.add(new Component[]{numeroPanel, nomPanel, prenomPanel, villePanel});

        pack();
    }

    public ClientView(Client client) {
        this(client, false);
    }

    @Override
    public void saveChanges() throws DuplicateException {
        // If the client is new, adding a NEW instance of Client so we can change the identifier(numero).
        if (this.isNew) {
            Magasin.getInstance().addClient(new Client(
                    this.numeroField.getText(),
                    this.nomField.getText(),
                    this.prenomField.getText(),
                    this.villeField.getText()));
        }
        client.setNom(this.nomField.getText());
        client.setPrenom(this.prenomField.getText());
        client.setVille(this.villeField.getText());
        super.saveChanges();
    }
}
