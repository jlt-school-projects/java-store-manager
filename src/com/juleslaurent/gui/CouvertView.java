package com.juleslaurent.gui;

import com.juleslaurent.codebehind.Couvert;
import com.juleslaurent.codebehind.DuplicateException;
import com.juleslaurent.codebehind.Magasin;
import com.juleslaurent.codebehind.Matiere;

import javax.swing.*;
import java.awt.*;

/**
 * Generic view for Couvert objects.
 * Created by Jules LAURENT on 27/03/2017.
 * Last updated on 16/04/17.
 */
public class CouvertView extends ArticleView {

    private JTextField matiereField;
    CouvertView(Couvert article,Boolean isNew)
    {
        super(article,isNew);
        this.matiereField = new JTextField(article.getMatiere().toString());
        MyLinePanel matierePanel = new MyLinePanel();
        matierePanel.add(new Component[] {new MyLabel("Matière : "),this.matiereField});
        this.form.add(matierePanel);
        pack();
    }

    CouvertView(Couvert article)
    {
        this(article,false);
    }


    @Override
    public void saveChanges() throws DuplicateException{
        ((Couvert)this.articleEnLot).setMatiere(Matiere.valueOf(this.matiereField.getText()));
        if(this.isNew)
        {
            Magasin.getInstance().addArticle(new Couvert(this.refField.getText(), this.intituleField.getText(),
            this.marqueField.getText(), Float.valueOf(this.prixUField.getText()), Matiere.valueOf(this.matiereField.getText())));
        }
        super.saveChanges();
    }
}
