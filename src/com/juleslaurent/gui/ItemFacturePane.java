package com.juleslaurent.gui;

import com.juleslaurent.codebehind.ItemFacture;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Specific TablePane with a "Add" button to billed items display.
 * Created by Jules LAURENT on 06/04/2017.
 */
public class ItemFacturePane extends TablePane {
    ItemFacturePane(MyAbstractTableModel modele)
    {
        super(modele);
        JButton addButton = new JButton("Ajouter");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ItemFacture tmp = new ItemFacture();
                modele.getItems().add(tmp);
                new ItemFactureView(tmp);
                refresh();
            }
        });
        this.boutonPanel.add(addButton);
    }
}
