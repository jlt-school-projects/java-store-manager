package com.juleslaurent.gui;

import com.juleslaurent.codebehind.Facture;
import com.juleslaurent.codebehind.ItemFacture;

/**
 * Table model to display ItemFacture (billed items).
 * Created by Jules LAURENT on 06/04/2017.
 * Last updated on 16/04/17.
 */
public class ItemFactureTable extends MyAbstractTableModel {
    private Facture facture;
    ItemFactureTable(Facture facture)
    {
        this.names = new String[]{"QUANTITE","REF","PRIX UNIT","SOUS TOTAL"};
        this.facture = facture;
        this.items = this.facture.getArticlesFactures();
    }

    @Override
    public int getRowCount() {
        return this.items.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        switch (i1)
        {
            case 0:
                return ((ItemFacture)this.items.get(i)).getNbItems();
            case 1:
                return ((ItemFacture)this.items.get(i)).getArticle().getRef();
            case 2:
                return ((ItemFacture)this.items.get(i)).getArticle().getPrixUnit();
            case 3 :
                return ((ItemFacture)this.items.get(i)).getNbItems() * ((ItemFacture)this.items.get(i)).getArticle().getPrixUnit();
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int i) {
        return this.names[i];
    }

}

