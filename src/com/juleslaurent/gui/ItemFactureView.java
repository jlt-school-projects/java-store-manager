package com.juleslaurent.gui;

import com.juleslaurent.codebehind.Article;
import com.juleslaurent.codebehind.DuplicateException;
import com.juleslaurent.codebehind.ItemFacture;
import com.juleslaurent.codebehind.Magasin;

import javax.swing.*;
import java.awt.*;

/**
 * Generic view for billed items objects.
 * Created by Jules LAURENT on 06/04/2017.
 * Last updated on 16/04/17.
 */
public class ItemFactureView extends View {
    private JComboBox articlejComboBox;
    private MyIntegerField nbItemsField = new MyIntegerField("0");

    private ItemFacture itemFacture;
    public ItemFactureView(ItemFacture itemFacture)
    {
        if(isNew)
            this.setTitle("Nouv. item");
        else
            this.setTitle("Item");

        this.itemFacture = itemFacture;
        ((JButton)this.boutons.getComponent(0)).setText("Ajouter");

        this.articlejComboBox = new JComboBox(Magasin.getInstance().getArticles().toArray());
        if(this.itemFacture.getArticle() != null)
        {
            this.articlejComboBox.setSelectedItem(this.itemFacture.getArticle());
            this.nbItemsField.setText(String.valueOf(this.itemFacture.getNbItems()));
        }


        MyLinePanel itemPanel = new MyLinePanel();
        itemPanel.add(new Component[]{new MyLabel("Article"),this.articlejComboBox});
        MyLinePanel nbItemsPanel = new MyLinePanel();
        nbItemsPanel.add(new Component[]{new MyLabel("Nombre d'items"),this.nbItemsField});

        this.form.add(new Component[]{itemPanel,nbItemsPanel});
        pack();

    }

    @Override
    public void saveChanges() throws DuplicateException
    {
            this.itemFacture.setArticle((Article) this.articlejComboBox.getSelectedItem());
            this.itemFacture.setNbItems(Integer.valueOf(this.nbItemsField.getText()));
            super.saveChanges();
    }
}
