package com.juleslaurent.gui;

import com.juleslaurent.codebehind.Article;
import com.juleslaurent.codebehind.DuplicateException;
import com.juleslaurent.codebehind.Lot;
import com.juleslaurent.codebehind.Magasin;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Generic view for package (Lot) objects.
 * Created by Jules LAURENT on 27/03/2017.
 * Last updated on 16/04/17.
 */
public class LotView extends ArticleView {
    private MyFloatField ratioField;
    private MyIntegerField nbArticlesField;
    private JComboBox articleComboBox;

    LotView() {

    }

    LotView(Lot lot, boolean isNew) {
        super(lot, isNew);
        this.intituleField.setEditable(false);
        this.prixUField.setEditable(false);
        this.articleComboBox = new JComboBox(Magasin.getInstance().getArticles().toArray());
        this.articleComboBox.setSelectedItem(((Lot) this.articleEnLot).getArticle());
        MyLinePanel articleComboPanel = new MyLinePanel();
        articleComboPanel.add(new Component[]{new MyLabel("Article : "), articleComboBox});

        this.marqueField.setEditable(false);
        nbArticlesField = new MyIntegerField(String.valueOf(lot.getNbArticles()));
        MyLinePanel nbArticlesPanel = new MyLinePanel();
        nbArticlesPanel.add(new Component[]{new MyLabel("Nombre d'articles"), nbArticlesField});

        ratioField = new MyFloatField(String.valueOf(lot.getRatioReduction()));
        MyLinePanel ratioPanel = new MyLinePanel();
        ratioPanel.add(new Component[]{new MyLabel("Ratio"), ratioField});

        this.form.add(new Component[]{articleComboPanel, nbArticlesPanel, ratioPanel});


        KeyListener priceFactorsUpdated = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {
            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
                updatePriceFactors();
            }
        };
        ratioField.addKeyListener(priceFactorsUpdated);
        nbArticlesField.addKeyListener(priceFactorsUpdated);

        ActionListener updateArticleInfos = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ((Lot) articleEnLot).setArticle((Article) articleComboBox.getSelectedItem());
                intituleField.setText(((Lot) articleEnLot).getArticle().getIntitule());
                marqueField.setText(((Lot) articleEnLot).getArticle().getMarque());
                updatePriceFactors();
            }
        };
        articleComboBox.addActionListener(updateArticleInfos);
        pack();
    }

    LotView(Lot lot) {
        this(lot, false);
    }

    @Override
    public void saveChanges() throws DuplicateException {
        if (this.isNew) {

            Magasin.getInstance().addArticle(new Lot(this.refField.getText(), (Article) this.articleComboBox.getSelectedItem(),
                    Float.valueOf(this.ratioField.getText()), Integer.valueOf(this.nbArticlesField.getText())));

        }
        ((Lot) this.articleEnLot).setNbArticles(Integer.valueOf(this.nbArticlesField.getText()));
        ((Lot) this.articleEnLot).setRatioReduction(Float.valueOf(this.ratioField.getText()));
        super.saveChanges();
    }

    public void updatePriceFactors() {
        try {
            prixUField.setText(String.valueOf(
                    ((Lot) this.articleEnLot).calculPrixUnit(
                            ((Lot) this.articleEnLot).getArticle().getPrixUnit(),
                            Float.valueOf(this.ratioField.getText()),
                            Integer.valueOf(this.nbArticlesField.getText())
                    )
            ));
            prixUField.repaint();
        } catch (Exception NumberFormatException) {
            new JOptionPane().showMessageDialog(null, "Veuillez entrer un nombre", "Attention ! ", JOptionPane.WARNING_MESSAGE);
        }
    }
}
