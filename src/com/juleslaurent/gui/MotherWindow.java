package com.juleslaurent.gui;

import com.juleslaurent.codebehind.Magasin;

import javax.swing.*;
import java.awt.*;

/**
 * Main/mother window.
 * Created by Jules LAURENT on 03/04/2017.
 * Last updated on 16/04/17.
 */
public class MotherWindow extends JFrame{
    private TablePane articlePane = new TablePane(new ArticlesTable());
    private TablePane clientPane = new TablePane(new ClientTable());
    private TablePane facturePane = new TablePane(new BillsTable());
    public MotherWindow()
    {
        this.setJMenuBar(new MyMenuBar());
        this.setTitle(Magasin.getInstance().getNom());
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(Toolkit.getDefaultToolkit().getScreenSize()); //Fenetre de la taille de l'ecran

        this.setContentPane(new JTabbedPane());
        this.getContentPane().add("Articles",this.articlePane);
        this.getContentPane().add("Clients",this.clientPane);
        this.getContentPane().add("Factures",this.facturePane);
        this.setVisible(true);

    }

    public void refresh()
    {
        articlePane.refresh();
        clientPane.refresh();
        facturePane.refresh();
    }
}
