package com.juleslaurent.gui;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

/**
 * Created by Jules LAURENT on 04/04/2017.
 */
public abstract class MyAbstractTableModel extends AbstractTableModel {
    protected ArrayList items;
    protected String[] names;

    public ArrayList getItems() {
        return items;
    }
}
