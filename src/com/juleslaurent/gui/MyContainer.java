package com.juleslaurent.gui;

import java.awt.*;

/**
 * Container with multiple component adding function.
 * Created by Jules LAURENT on 27/03/2017.
 */
public class MyContainer extends Container {
    MyContainer()
    {

    }

    /**
     * Allow us to add many components in one line.
     * @param components contains the components to add.
     * @return the updated target component.
     */
    public Component add(Component[] components) {
        for(Component o : components)
        {
            super.add(o);
        }
        return this;
    }
}
