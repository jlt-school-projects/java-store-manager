package com.juleslaurent.gui;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Text field accepting digits n. dots only.
 * Created by Jules LAURENT on 03/04/2017.
 * Last updated on 16/04/17.
 * JavaDoc : TRUE.
 */
public class MyFloatField extends JTextField {
    private KeyListener integrityChecker =  new KeyListener() {
        @Override
        public void keyTyped(KeyEvent keyEvent) {
        }

        @Override
        public void keyPressed(KeyEvent keyEvent) {}

        @Override
        public void keyReleased(KeyEvent keyEvent) {
            JTextField field = (JTextField)keyEvent.getSource();
            if(field.getText().equals(""))
                field.setText("0");
            try
            {
                Float.valueOf(field.getText());
            }catch (Exception NumberFormatException )
            {
                JOptionPane.showMessageDialog(null, "Veuillez entrer un nombre", "Attention ! ", JOptionPane.WARNING_MESSAGE);
            }
        }
    };
    MyFloatField()
    {
        this("");
    }

    /**
     *
     * @param s Number to display.
     */
    MyFloatField(String s)
    {
        super(s);
        this.addKeyListener(this.integrityChecker);
    }

}
