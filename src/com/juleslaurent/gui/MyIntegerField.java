package com.juleslaurent.gui;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.reflect.Executable;

/**
 * Text field accepting digits only.
 * Created by Jules LAURENT on 03/04/2017.
 * Last updated on 16/04/17.
 */
public class MyIntegerField extends JTextField{
    KeyListener integrityChecker =  new KeyListener() {
        @Override
        public void keyTyped(KeyEvent keyEvent) {
        }

        @Override
        public void keyPressed(KeyEvent keyEvent) {}

        @Override
        public void keyReleased(KeyEvent keyEvent) {
            JTextField field = (JTextField)keyEvent.getSource();
            if(field.getText().equals(""))
                field.setText("0");
            try
            {
                Integer.valueOf(field.getText());
            }catch (Exception NumberFormatException )
            {
                JOptionPane.showMessageDialog(null, "Veuillez entrer un nombre", "Attention ! ", JOptionPane.WARNING_MESSAGE);
            }
        }
    };

    /**
     *
     * @param s Number to display.
     */
    MyIntegerField(String s)
    {
        super(s);
        this.addKeyListener(this.integrityChecker);
    }
}
