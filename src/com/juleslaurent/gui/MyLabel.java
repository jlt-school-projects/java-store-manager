package com.juleslaurent.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Label with a width depending on the screen size.
 * Created by Jules LAURENT on 27/03/2017.
 * Last updated on 16/04/17.
 */
public class MyLabel extends JLabel {
    MyLabel()
    {
        super();
    }
    MyLabel(String s)
    {
        super(s);
        Dimension dim = new Dimension((int)Toolkit.getDefaultToolkit().getScreenSize().getWidth()/10,this.getHeight());
        this.setPreferredSize(dim);
    }

}
