package com.juleslaurent.gui;

import javax.swing.*;

/**
 * Linear axed panel.
 * Created by Jules LAURENT on 27/03/2017.
 */
public class MyLinePanel extends MyContainer {
    MyLinePanel()
    {
        this.setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
    }
}
