package com.juleslaurent.gui;

import com.juleslaurent.codebehind.*;

import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Jules LAURENT on 28/03/2017.
 */
public class MyMenuBar extends JMenuBar {

    MyMenuBar()
    {
        super();
        JMenu nouveauMenu = new JMenu("");
        nouveauMenu.setIcon(new ImageIcon(getClass().getResource("/new.png")));
        JMenuItem nouveauCasserolle = new JMenuItem("Casserolle");
        JMenuItem nouveauLot = new JMenuItem("Lot");
        JMenuItem nouveauCouvert = new JMenuItem("Couvert");
        JMenuItem nouveauClient = new JMenuItem("Client");
        JMenuItem nouveauFacture = new JMenuItem("Facture");

        nouveauMenu.add(nouveauCasserolle);
        nouveauMenu.add(nouveauCouvert);
        nouveauMenu.add(nouveauLot);
        nouveauMenu.add(nouveauClient);
        nouveauMenu.add(nouveauFacture);

        JMenu refresh = new JMenu();
        refresh.setIcon(new ImageIcon(getClass().getResource("/refresh.png")));
        JMenu save = new JMenu();
        System.out.println(getClass().getResource("/save.png"));
        save.setIcon(new ImageIcon(getClass().getResource("/save.png")));

        refresh.addMenuListener(new MenuListener() {
            @Override
            public void menuSelected(MenuEvent menuEvent) {
                System.out.println("blabla");
                ((MotherWindow)getTopLevelAncestor()).refresh();
            }

            @Override
            public void menuDeselected(MenuEvent menuEvent) {

            }

            @Override
            public void menuCanceled(MenuEvent menuEvent) {

            }

        });

        nouveauCasserolle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new CasseroleView(new Casserole(),true);
            }
        });

        nouveauCouvert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new CouvertView(new Couvert(),true);
            }
        });

        nouveauLot.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new LotView(new Lot(),true);
            }
        });

        nouveauClient.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new ClientView(new Client(),true);
            }
        });

        nouveauFacture.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new BillView(new Facture(),true);
            }
        });
        save.addMenuListener(new MenuListener() {
            @Override
            public void menuSelected(MenuEvent menuEvent) {
                Magasin.save();
            }

            @Override
            public void menuDeselected(MenuEvent menuEvent) {

            }

            @Override
            public void menuCanceled(MenuEvent menuEvent) {

            }
        });
        this.add(nouveauMenu);
        this.add(refresh);
        this.add(save);
    }
}
