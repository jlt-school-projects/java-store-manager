package com.juleslaurent.gui;

import com.juleslaurent.codebehind.*;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Basicely a table created via a model and some buttons to interact with it.
 * Created by Jules LAURENT on 04/04/2017.
 */
public class TablePane extends JPanel {
    protected MyAbstractTableModel modele;
    protected JTable table;
    protected MyLinePanel boutonPanel = new MyLinePanel();


    TablePane(MyAbstractTableModel modele)
    {
        this.modele = modele;
        this.table = new JTable(this.modele);

        this.setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS)); //Positionnement des elements en colonnes
        this.add(new JScrollPane(this.table));


        JButton deleteButton = new JButton("Supprimer");
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int[] selection = table.getSelectedRows();
                for(int i = selection.length - 1;i >= 0; i--)
                {
                    modele.getItems().remove(modele.getItems().get(selection[i]));
                    refresh();
                }
            }
        });
        this.boutonPanel.add(deleteButton);
        this.add(this.boutonPanel);


        this.table.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                Object item = modele.getItems().get(table.getSelectedRow());
                System.out.println(item.getClass());
                if (item.getClass() == Casserole.class) {
                    new CasseroleView((Casserole)item);
                }
                else if (item.getClass() == Couvert.class)
                {
                    new CouvertView((Couvert)item);
                }
                else if(item.getClass() == Lot.class)
                {
                    new LotView((Lot)item);
                }
                else if(item.getClass() == Client.class)
                {
                    new ClientView((Client)item);
                }
                else if(item.getClass() == Facture.class)
                {
                    new BillView((Facture)item);
                }
                else if(item.getClass() == ItemFacture.class)
                {
                    new ItemFactureView((ItemFacture)item);
                }

            }
            @Override
            public void mousePressed(MouseEvent mouseEvent) {}

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {}

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {}

            @Override
            public void mouseExited(MouseEvent mouseEvent) {}
        });

        this.setVisible(true);
    }

    public void refresh()
    {
        modele.fireTableStructureChanged();
        table.revalidate();
    }


}
