package com.juleslaurent.gui;

import com.juleslaurent.codebehind.DuplicateException;
import com.sun.org.apache.bcel.internal.generic.DUP;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Generic object view, with fields reserved space and save button.
 * Created by Jules LAURENT on 04/04/2017.
 * Last updated on 16/04/17.
 * JavaDoc : TRUE.
 */
public abstract class View extends JFrame {
    protected Container boutons = new Container();
    protected MyContainer form = new MyContainer();
    protected Boolean isNew = false;

    public View() {
        //Setting the layouts so the elements align with each others vertically.
        this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.PAGE_AXIS));
        this.form.setLayout(new BoxLayout(form, BoxLayout.PAGE_AXIS));

        //Creating and setting up the save button.
        JButton saveButton = new JButton("Enregistrer");
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    saveChanges();
                } catch (DuplicateException ex) {
                    JOptionPane.showMessageDialog(null, ex, "Opération annulée ", JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        //Adding the button in a panel so we'll be able to add more buttons if we have to.
        this.boutons.setLayout(new BoxLayout(this.boutons, BoxLayout.LINE_AXIS));
        this.boutons.add(saveButton);

        this.getContentPane().add(form);
        this.getContentPane().add(boutons);
        this.setVisible(true);
    }

    /**
     * Save the fields value into the associated object fields.
     * @throws DuplicateException
     */
    public void saveChanges() throws DuplicateException {
        //Close the frame.
        this.setVisible(false);
        this.dispose();
    }
}
